
var functions = {
  /**
   * @name 判断传入地址的图片是否加载成功
   * @param {str} imgurl 
   * @param {function} callback 
   */
  imgLoad(imgurl, callback) {
    var img = new Image()
    img.src = imgurl
    if (callback) {
      callback()
    }
    return img.complete
  },

  /**
   * @name 倒计时
   * @param {int} time 
   * @param {function} callback
   */
  countDown(time, callback) {
    var timeDown = setInterval(() => {
      // return time
      if (time == 0) {
        clearInterval(timeDown)
        if (callback) callback()
      } else {
        time--
      }
    }, 1000)
  },

  /**
   * @name 获取当前路由path
   * @param {*} vue 
   */
  getRouterPath(vue) {
    var routerPath = vue.$router.history.current.path
    return routerPath
  },

  /**
   * @name 获取当前路由中携带的参数，返回param或者返回全部的参数
   * @param {*} vue 
   * @param {str} param 
   */
  getRouterParams(vue, param) {
    if (param) {
      return vue.$router.history.current.params.param
    } else {
      return vue.$router.history.current.params
    }
  },

  getBacImg(vue) {
    return 'background:url(' + localStorage.getItem("homepageImg") + ');background-position-x:' + vue.$store.state.homePageBac.positionX + ';background-position-y:' + vue.$store.state.homePageBac.positionY + ';background-size:' + vue.$store.state.homePageBac.bacSize
  },

  /**
   * @name 从array中获取符合的obj
   */
  getRightItem(array, key, value) {
    for (var item of array) {
      if (item[key] == value) {
        return item
      }
    }
  },

  /**
   * @name 信息提示
   * @param {*} setting 基础配置信息
   * @description 通用
   */
  toMessage(setting={status:0,msg:"成功"}){
    if(!window.Element)console.warn("没有引入element；提示信息的ui使用 vant");
    if(!window._bus)throw new Error('没有定义总线 _bus ');
    const msgType = window.msgType||(window.Element?"element":"vant");
    let type = ["success","warning","error"][setting.status];
    let message = setting.msg;
    switch(msgType){
      case "element":window._bus.$message({type,message});break;
      case "vant": window._bus.$toast[setting.status==0?"success":"fail"](message);break;
    }
  },

  /**
   * @name 调用方法序列
   * @param {*} array 
   */
  async orderMethods(array = []) {
    for (var i = 0; i < array.length; i++) {
      await array[i]()
    }
  },

  /**
   * @name 检查参数
   * @param {*} model 数据模板
   * @param {*} params 检查的数据
   */
  checkParams(model,params){
    if(!model)throw new Error('数据模板不能为空')
    if(!params)throw new Error('检查的数据不能为空')
    var result=false
    model.forEach(ele=>{
      if(ele.checked&&!params[ele.prop]){
        _g.toMessage({status:1,msg:ele.label+"不能为空"})
        result = true
      }
    })
    return result
  },
  
  /**
   * @name apiPost 封装post方法
   * @param {*} url 
   * @param {*} data 
   */
  async apiPost(url,data) {
    var path = PATH + url
    var result = null
    try {
      await _axios.post(path, qs.stringify(data)).then(res => result = res);
    }
    catch (err) {
      var message = "出问题了"
      switch (err.toString().split(" ").slice(-1)[0]) {
        case "404": message = "访问地址不正确"; break;
        case "500": message = "后台出错"; break;
      }
      _g.toMessage({ type: "warning", msg:message })
    }
    return await result ? result.data : null;
  },

  /**
   * @name setArrayKey 设置数组option key=value
   * @param {array} array 
   * @param {obj} {} ==> {key,value}
   */
  setArrayKey(array, { key, value }) {
    array.forEach(ele => { ele[key] = value })
  },

  /**
   * @name 生成[num]的数组
   * @param {*} num 
   */
  getNumArray(num) {
    return new Array(num).fill("").map((val, index) => (index + 1))
  },

  // 全屏
  /**
   * @name 全屏显示局部元素
   * @param {*} id 
   * @param {*} vue 
   * @param {function} callback
   */
  fullScreen(id,vue,callback) {
    if(!id)throw new Error("全屏元素的id必填")

    let fullarea = document.getElementById(id);
    
    if (vue.isFullScreen) {
      // 退出全屏
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      }
    } else {
      // 进入全屏
      if (fullarea.requestFullscreen) {
        fullarea.requestFullscreen();
      } else if (fullarea.webkitRequestFullScreen) {
        fullarea.webkitRequestFullScreen();
      } else if (fullarea.mozRequestFullScreen) {
        fullarea.mozRequestFullScreen();
      } else if (fullarea.msRequestFullscreen) {
        // IE11
        fullarea.msRequestFullscreen();
      }
    }

    if(callback){
      setTimeout(()=>{
        callback()
      },200)
    }
    
    vue.$set(vue,'isFullScreen',!vue.isFullScreen)
  },

  /**
   * @name 单个数字转换为大写
   * @param {*} word 
   */
  toUp(word){
    switch (word) {
      case "0": return "零"
      case "1": return "壹"
      case "2": return "贰"
      case "3": return "叁"
      case "4": return "肆"
      case "5": return "伍"
      case "6": return "陆"
      case "7": return "柒"
      case "8": return "捌"
      case "9": return "玖"
    }
  },

  /**
   * @name 动态载入js
   * @param {*} src 
   * @param {*} callback 
   */
  loadJs(src,callback){
    var head= document.getElementsByTagName('head')[0]; 
    var script= document.createElement('script'); 
    script.type= 'text/javascript'; 
    script.onreadystatechange= function () { 
    if (this.readyState == 'complete') 
      callback(); 
    } 
    script.onload= function(){ 
      callback(); 
    } 
    script.src= src; 
    head.appendChild(script);
  },

  /**
   * @name 拓展方法
   * @param {*} name 
   * @param {*} method 
   */
  fn(name, method) {
    if (!method) throw new Error("拓展方法未定义")
    if (Object.keys(this).includes(name)) throw new Error("方法名" + name + "重复")
    this[name] = method
  }

}

export default functions