import _g from './global.js'
import _axios from 'axios'
import Vue from 'vue'
import qs from 'qs'
window._vue=Vue
window._g=_g  //公共方法
window._axios=_axios
window._bus=new Vue()
window.qs=qs
window.PATH=window.location.href.indexOf('192.168.10.')>-1?"/api/":"/"

window.localSaved={
  savedMethods:"saved_methods",
  savedMoneyInfo:"saved_moneyinfo",
}