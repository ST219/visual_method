let config={
  notice:{
    title:"提示",
    content:"<span>不提供数据存储，不计算后期效益，只计较当前。<span class='color-red'>填写的数据暂时没有删除，请谨慎填写</span>(仅供娱乐参考)</span>"
  },

  modelData:[
    {prop:"name",icon:"browsing-history-o color-blue",label:"公司名称",},
    {proportion:"",prop:"base",icon:"balance-o color-blue",label:"基本薪资(元)",right_icon:"question-o color-red",type:"number"},
    {proportion:"",prop:"workdays",icon:"wap-home color-blue",label:"工作日(天)",type:"number"},
    {proportion:"",prop:"subsidy",icon:"cash-on-deliver color-blue",label:"补贴累计(元)",type:"number"},
    {proportion:"",prop:"annualLeave",icon:"closed-eye color-blue",label:"年假(天)",type:"number"},
    {proportion:"",prop:"socialsecurity",icon:"bulb-o color-blue",label:"社保(元)",type:"number"},
    {proportion:"",prop:"fund",icon:"cashier-o color-blue",label:"公积金(元)",type:"number"},
  ]
}

export default config