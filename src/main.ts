import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'


// 初始化
import './assets/css/reset.css'
import './assets/css/common.css'
import './assets/scss/color.scss'
import './plugs/plugs.css'
import './assets/js/constant.js'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
