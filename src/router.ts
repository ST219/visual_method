import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path:"/",
      redirect:"/admin/login"
    },{
      path:"/admin/login",
      component: ()=>import("./views/pages/login/checklogin.vue")
    },{
      path:'/visualization/methods',
      component: ()=>import("./views/pages/visualization/methods/methods.vue")
    },{
      path:"/visualization/urls",
      component: ()=>import("./views/pages/visualization/urls/urls.vue")
    },{
      path: '/error/404',
      component: ()=>import("./views/pages/error/error.vue")
    },

    {
      path:"/visualization/formoney",
      component: ()=>import("./views/pages/visualization/formoney/formoney.vue")
    },{
      path:"/visualization/formoney/echarts",
      component: ()=>import("./views/pages/visualization/formoney/echart.vue")
    }
  ]
})
