let operateMethods = require('./data/methods/index')
let operateUrls = require('./data/urls/index')
let checkLogin = require('./data/login/login') 
let _g = require('./common.js')
let querystring = require('querystring');

let menuRouters=[
  ['/userlogin',checkLogin],
  ['/visualization/methods/list',operateMethods],
  ['/visualization/urls/list',operateUrls]
]

const routes=function(req,res){
  _g.getParams(req,res,menuRouters)
  _g.log(req.url)
}
module.exports=routes;
