let querystring = require('querystring');

/**
 * @name 同步
 */
async function orderAsync(array){
  for(let i = 0 ; i<array.length;i++){
    await array[i]
  }
}

/**
 * @name 获取post传过来中的参数并返回
 * @param {*} req 
 */
async function getParams(req,res,menuRouters){
  let goal = new Map(menuRouters).get(req.url)
  let params=""
  req.on('data', function (chunk) {
    params += chunk;
  });
  req.on('end', function () {
    params = decodeURI(params);
    var dataObject = querystring.parse(params);
    goal(dataObject,res)
  });
}

/**
 * @name 打印
 */
function log(str){
  console.log(str)
}

module.exports={orderAsync,getParams,log}