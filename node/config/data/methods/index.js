const fs = require('fs');
var iconv = require('iconv-lite')
const getList = function(req,res){
  let data = fs.readFileSync('./config/data/methods/build_in.js');
  // var strr = iconv.decode(data,'GB2312');
  var buffer = Buffer.from(data)
  var methods = buffer.toString().replace(/\s{2,}/g,"").replace(/\/\*.{10,}?\//g,"").match(/{.*}/g)[0].slice(1,-1).split(/\B,\b(?!\d)/g)
  var explains= buffer.toString().replace(/\s{2,}/g,"").match(/\/\*.{10,}?\//g);
  res.end(JSON.stringify({methods,explains}))
}
module.exports=getList