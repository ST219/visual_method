const http = require('http')
const config = require('./config/config')
const routes = require('./config/routes')

http.createServer((req,res)=>{
  res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8' ,"Access-Control-Allow-Origin": !config.useCros?"*":""});
  routes(req,res)
}).listen(config.port)

console.log(`服务已启动，监听${config.port}`)